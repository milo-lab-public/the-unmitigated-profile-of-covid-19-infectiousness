# The unmitigated profile of COVID-19 infectiousness

This repository is accompanying the paper ["The unmitigated profile of COVID-19 infectiousness"]()

Ron Sender†, Yinon M. Bar-On† , Sang Woo Park, Elad Noor, Jonathan Dushoffd, Ron Milo*

†equal contribution

*corresponding aothur: RM ron.milo@weizmann.ac.il

This repository contains four jupyter notebooks and two folders. To prodoce the results they should be run in this exact order: 

* **[`Preprocess.ipynb`](./Preprocess.ipynb)|** This notebook is responsible for preprocessing of the data. It loads the csv with the transmission pairs datasets and filter it to discard possible duplicates. It also use a Bayseian inference method to estiamte the mean serial interval as a function of the day in which the infector developed symptoms 

* **[`Inference_functions.ipynb`](./Inference_functions.ipynb)|** This notebook contains the functions that enable running the maximum likelihood analysis for the distribution of the generation intervals. It also contains many functions for handeling the resultsing distribution. For example, given infectivity model and parameters it enables the presentation via figures and derivation of further results (for example, R0) 

* **[`Inference_operation.ipynb`](./Inference_operation.ipynb)|** This notebook contains the functions and preform the maximum likelihood analysis. It rely on the preprocessed data and uses the functions provided in the Inference_function notebook. It save the results and the directory [`Data/Results`](./Data/Results) so they can be plotted.

* **[`Figures_generator.ipynb`](./Figures_generator.ipynb)|** This notebook produce all the figures and supplemental figures of the paper (except from the chart in Figure 1). It uses the results of the analysis produced by [`Inference_operation.ipynb`](./Inference_operation.ipynb) notebook and saved to [`Data/Results`](./Data/Results), as well as the preprocessed data.

* **[`Data/`](./Data)|** The raw data ([`Data/Inputs`](./Data/Inputs))as well as the preprocess datasets ([`Data/Processed_datasets`](./Data/Processed_datasets)) and the results of the analysis ([`Data/Results`](./Data/Results)).

* **[`Figures/`](./Figures)|** The directory in which all the figures are saved by [`Figures_generator.ipynb`](./Figures_generator.ipynb)
